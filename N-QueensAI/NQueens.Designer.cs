﻿namespace N_QueensAI
{
    partial class NQueens
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NQueens));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.QueenCountTextBox = new System.Windows.Forms.TextBox();
            this.SolverButton = new System.Windows.Forms.Button();
            this.ChessPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(236, 59);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(99, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Queen Count:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl2.Location = new System.Drawing.Point(180, 12);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(404, 31);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "N-QUEENS PROBLEM SOLVER";
            // 
            // QueenCountTextBox
            // 
            this.QueenCountTextBox.Location = new System.Drawing.Point(341, 61);
            this.QueenCountTextBox.Name = "QueenCountTextBox";
            this.QueenCountTextBox.Size = new System.Drawing.Size(100, 21);
            this.QueenCountTextBox.TabIndex = 2;
            // 
            // SolverButton
            // 
            this.SolverButton.BackColor = System.Drawing.Color.LightGray;
            this.SolverButton.ForeColor = System.Drawing.Color.Black;
            this.SolverButton.Location = new System.Drawing.Point(268, 88);
            this.SolverButton.Name = "SolverButton";
            this.SolverButton.Size = new System.Drawing.Size(136, 23);
            this.SolverButton.TabIndex = 3;
            this.SolverButton.Text = "SOLVE";
            this.SolverButton.UseVisualStyleBackColor = false;
            this.SolverButton.Click += new System.EventHandler(this.SolverButton_Click);
            this.SolverButton.MouseLeave += new System.EventHandler(this.SolverButton_MouseLeave);
            this.SolverButton.MouseHover += new System.EventHandler(this.SolverButton_MouseHover);
            // 
            // ChessPanel
            // 
            this.ChessPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ChessPanel.Location = new System.Drawing.Point(66, 139);
            this.ChessPanel.Name = "ChessPanel";
            this.ChessPanel.Size = new System.Drawing.Size(627, 506);
            this.ChessPanel.TabIndex = 4;
            // 
            // NQueens
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(776, 681);
            this.Controls.Add(this.ChessPanel);
            this.Controls.Add(this.SolverButton);
            this.Controls.Add(this.QueenCountTextBox);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "NQueens";
            this.Text = "NQueens";
            this.Load += new System.EventHandler(this.NQueens_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.TextBox QueenCountTextBox;
        private System.Windows.Forms.Button SolverButton;
        private System.Windows.Forms.Panel ChessPanel;
    }
}