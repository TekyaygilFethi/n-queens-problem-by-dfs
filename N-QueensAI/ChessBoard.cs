﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace N_QueensAI
{
    class ChessBoard
    {
        private int ExpectedQueenCount;//how many queens must be placed to reach goal state
        private int[] board;
        private int PlacedQueenCount; //holds the number of the queens placed(how many queens placed to board)
        public int ClickCountFromNextButton = 0;
        public int ClickControlForNextButton = 0;
        
        public ChessBoard(int ExpectedQueenCount)
        {
            PlacedQueenCount = 0; //we have 0 queen placed at the beginning
            this.ExpectedQueenCount = ExpectedQueenCount;//assign the parameter value to use this varable in this class
            board = new int[ExpectedQueenCount];//this will hold the row values of the placed queens(which row is it placed)

            for (int i = 0; i < board.Length; i++)//set all values to 0 at the beginning
                board.SetValue(0, i);
        }

        public void PlaceQueen(int Column)//This method places the current queen to the board
        {
            if (Column >= 0 && Column < ExpectedQueenCount)
            {
                board[PlacedQueenCount] = Column;//Current queen will be placed at column {Column}.
                PlacedQueenCount++;//One queen is placed.Increase the placed queen number.
            }
        }

        public void UnPlaceQueen()//Unplace the previously placed queens to make the correct placements.
        {
            if (PlacedQueenCount > 0)
            {
                PlacedQueenCount--;//we don't have to make 0 the relevant index of the board because it reaches the index with PlacedQueens variable and because of that values in the board array can be overwritten.
            }
        }

        public bool CanBePlaced(int Column)//This method controls if the queen can be placed to the column which given in parameter
        {
            for (int QueenPlace = 0; QueenPlace < PlacedQueenCount; QueenPlace++)//Check current queen with the all previous placed queens
            {
                if ((board[QueenPlace] == Column) || Math.Abs(Column - board[QueenPlace]) == (PlacedQueenCount - QueenPlace))//check if previous queens are in the same row with the given column or if tey are in the same diagonal(for diagonal check if the row and column distance are equal,it is diagonal)(PlacedQueen-QueenRowPlace means that ever queen placed order by row(first queen placed at first row etc.)and it controls the distance of teh row from the all previous queens to current queen row.If one of these two conditions are met return false which means queen can not be placed
                    return false;
            }

            return true;//If condiitions are met(if queen can be placed without conflict with the previous placed queens) return true.
        }

        public bool IsFinished() //This methosd controls that if all the queens are placed to board
        {
            if (PlacedQueenCount == ExpectedQueenCount) //If placed queens' number is equal to expected placed queens' number it means the program reach the goal state and return true.Otherwise return false
                return true;
            else
                return false;
        }

        public int GetExpectedQueens()
        {
            return ExpectedQueenCount;
        }

        public void ShowChessBoard(Panel ChessPanel)//This method creates a visually representation of the chess board with picturebox component in the panel which given as parameter.
        {
            PictureBox[,] boardFloor = new PictureBox[ExpectedQueenCount, ExpectedQueenCount];//We will use the PictureBox to representation Chess Board row and columns.
            int width = ChessPanel.Width, height = ChessPanel.Height;//We get Width and Height of the Chess Panel.
            int xSize = (int)((double)width / (double)ExpectedQueenCount);//X dimesion Size of the Button
            int ySize = (int)((double)height / (double)ExpectedQueenCount);//Y dimension size of the Button

            for (int row = 0; row < boardFloor.GetLength(0); row++)
            {
                for (int column = 0; column < boardFloor.GetLength(1); column++)
                {
                    boardFloor[row, column] = new PictureBox();//Created the picture box element from all the elements of boardFloor array.

                    boardFloor[row, column].BorderStyle = BorderStyle.Fixed3D; // set every picture box's border.(Otherwise picture boxes will have no border and graphical interface will be insufficient.

                    boardFloor[row, column].Location = new Point(column * xSize , row * ySize);//place pictureboxes side by side(new picture box will be shift to right by the size of the picture box)

                    boardFloor[row, column].Size = new Size(xSize, ySize);//make every picturebox's size is equal and fit to panel as expected.

                    PaintBoardFloorPiece(ref boardFloor[row, column], row, column);//The pictureboxes painted to black and white respectively

                    boardFloor[row, column].Parent = ChessPanel;//All PictureBox items' parent set to panel which means all these components will be shown in this panel.(These are panel's controls now.)
                }
                    PlaceQueensToBoard(ref boardFloor, row, board);//This method will place queen to the where they belong in the goal state
                }
            }
        
        public void PlaceQueensToBoard(ref PictureBox[,] boardFloor, int row, int[] board)
        {
            for (int i = 0; i <= row; i++)
            {
                boardFloor[i, board[i]].Load(Application.StartupPath + "\\Queen.png");//We will place queen each row and column board[] array holds the columns of the placed queens.These queen images will represent the queens.
                boardFloor[i, board[i]].SizeMode = PictureBoxSizeMode.StretchImage;//In picturebox we must stretch the image to show image most correctly.
            }
        }
        
        public void PaintBoardFloorPiece(ref PictureBox boardFloorPiece, int row, int column)//This method paint the pictureboxes respectively and controls it with modulo operation.
        {
            if ((row + column) % 2 == 0)
            {
                boardFloorPiece.BackColor = Color.White;
            }
            else
                boardFloorPiece.BackColor = Color.Black;
        }


    }
}
