﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace N_QueensAI
{
    public partial class NQueens : DevExpress.XtraEditors.XtraForm
    {
        
        public NQueens()
        {
            InitializeComponent();
        }

        private void NQueens_Load(object sender, EventArgs e)
        {
        }
        
        protected void SolverButton_Click(object sender, EventArgs e)
        {
            SolveThePuzzle();
        }

        ChessBoard DFS(ChessBoard ChessBoard) //This method controls the board and queen status.It places queen and unplace queens if it's necessary.At last it returns the board or null according to goal state(explained before) is reached or not.
        {
            if (ChessBoard.IsFinished()) //If all the queens placed correctly return the chess board to itself
            {
                return ChessBoard;
            }
            else
            {
                for (int i = 0; i < ChessBoard.GetExpectedQueens(); i++) //check the state of selected queen with the all previous placed queens
                {
                    if (ChessBoard.CanBePlaced(i)) //can queen be placed correctly
                    {
                        ChessBoard.PlaceQueen(i);//place queen in board
                        ChessBoard resultBoard = DFS(ChessBoard); //call method recursively

                        if (resultBoard != null) //Check if all queens placed correctly if yes return board to SolveThePuzzle method
                            return resultBoard;
                        else //else unplace the last placed queen
                            ChessBoard.UnPlaceQueen();
                    }
                }
            }
            return null;
        }
        
        public void SolveThePuzzle()
        {
            ChessPanel.Controls.Clear(); //clear the panel controls if there are any controls(control means component like button,label etc.)

            if (QueenCountTextBox.TextLength != 0) //If an input entered to textbox
            {
                int ExpectedQueens = int.Parse(QueenCountTextBox.Text); //number of the queens that will be placed

                ChessBoard ChessBoard = new ChessBoard(ExpectedQueens); //Create an object from Chess Board Class which allows us to make board operations like place-unplace queen,check queen status and show the board
                
                ChessBoard result = DFS(ChessBoard);//DFS is the method allows us to place-unplace the queen and check the board status if it reaches the goal state.DFS calls methods from ChessBoard object.


                if (result != null)//DFS method returns the board if it reaches to goal state(all queeens are placed correctly).Otherwise it returns null
                {
                    result.ShowChessBoard(ChessPanel);//ShowChessBoard method creates a representation of the chess board in the panel which given as parameter to method.(The detail of this operation will be examined in the relevant section)
                }
                else
                    MessageBox.Show("Fail!");
            }
        }

        private void SolverButton_MouseHover(object sender, EventArgs e)
        {
            SolverButton.BackColor = Color.Blue;
            SolverButton.ForeColor = Color.White;
        }

        private void SolverButton_MouseLeave(object sender, EventArgs e)
        {
            SolverButton.BackColor = Color.LightGray;
            SolverButton.ForeColor = Color.Black;
        }
        
        
    }
}